$('.menu-key').on('click',function () {
  $('.menu-with-content').toggleClass('opened');
});

$('.menu-close').on('click',function () {
  $('.menu-with-content').toggleClass('opened');
});

$('.contact-key').on('click',function () {
  $('.contact-wrapper').toggleClass('opened');
});

$('.contact-close').on('click',function () {
  $('.contact-wrapper').toggleClass('opened');
});

//region Animated Text On Home Page
var tl = new TimelineMax({repeat:-1});
$('.animated-header-text span').each(function () {
  var chars = new SplitText($(this), {type:"chars"});
  chars = chars.chars;
  var splitTextTimeline = new TimelineMax();
  splitTextTimeline
    .staggerFrom(chars,1.2,{x:40,autoAlpha:0,ease: Power4.easeOut},0.05)
    .staggerTo(chars,1.1,{x:-40,autoAlpha:0,ease: Power2.easeIn},0.05,"+=.5");
  
  tl.add(splitTextTimeline,"+=.5");
});
//endregion Animated Text On Home Page
